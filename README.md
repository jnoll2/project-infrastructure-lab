---
author: Dr. John Noll
course: 7COM1085
date: '`\today`{=tex}'
header-includes: |
  ```{=tex}
  \usepackage{smartdiagram}
  \usesmartdiagramlibrary{additions}
  ```
institute: University of Hertfordshire
module: 7COM1085
module_leader: Dr. John Noll
subtitle: '7COM1085 -- Research Methods'
tables: true
term: 'spring-21'
title: 'Literature reviews -- Project infrastructure lab lab'
---

```{=tex}
\usepackage{smartdiagram}
\usesmartdiagramlibrary{additions}
```

Instructions for completing this practical exercise:

1.  Compile the lab instructions

    1.  If you are reading this *README.md* file from the web, you are
        not playing the game!

    Open a Command shell and change to the Git workspace containing this
    file, that you just cloned:

         #. Click the "windows" button, type "command," then open a "DOS
          box" (the black window).

         #. At the command prompt, type:

                 cd project-infrastructure-lab

             *Note:* If you did not clone the project-infrastructure-lab repository in
              your "home" directory, you should type:

                 cd \Users\yourusername\path\to\project-infrastructure-lab

             **IMPORTANT!**  replace "\\path\\to" above with the path to the
         directory into which you cloned the project-infrastructure-lab repository.

    1.  In the command window, type:

             instructions.html

        to view the instructions in a web browser.

        You can also read the instructions using Acroread by viewing
        *instructions.pdf*.

2.  Read and follow the instructions.
