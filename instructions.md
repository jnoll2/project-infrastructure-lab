!comment(-------------------------------------------)
!comment(  CAUTION! files generated from this file  )
!comment(  will only be correct if the workspace is )
!comment(  cloned within a topic directory.         )
!comment(-------------------------------------------)
---
author: #module_leader
date: #date
documentclass: hitec
fontfamily: opensans
fontsize: 12
...

## Project Infrastructure

#. Choose a member of your group to be the repository owner; this
 person's Bitbucket account will host the group repository, so he or
 she should be reliable.

#. The repository host team member should create a Bitbucket repository for your group.  Name your repository
"#module-groupXX", where "XX" is replaced by your group number.

    Please follow this naming convention _exactly_ as we will be
retrieving your repository automatically using a script to download
and examine your artifacts.  

    _Note:_ You _may_ include a hyphen ('-') between 'group' and the
number, as in "#modujle-group-XX"; we prefer no hyphen but the
grading scripts can handle either.


#. Invite #email(j.noll@herts.ac.uk) to become a member of your repository's
 team.

    #. Select "Repository settings" from the left menu, then "User and
     group access."

        ![Repository settings](bitbucket-settings.png){height=70%}\


    #. Enter #email(j.noll@herts.ac.uk) in the "Users" dialog.
    #. Select "Write" or "Admin" from the dropdown (it's necessary to grant write
access so we can give you feedback on your coursework by committing
files to your repository).

        ![User access](bitbucket-user-access.png){height=70%}\


    You need to enable at least "Write" access so we can push feedback
on your deliverables to your repository.

    #. Click the "Add" button.

    _Caution_: Make sure your repository is accessible to
#email(j.noll@herts.ac.uk). Since we will be using Bitbucket for coursework
submissions after this week, if you don't make it accessible, or don't
follow the naming convention, you won't receive credit for your
submissions.


#. Submit the URL for your Bitbucket repository 
 using the "Git (Bitbucket) repository URL" submission page on our Canvas site.

    **Note**: only _one_ group member needs to submit the URL on
      behalf of the entire group.

#. _Validate_ the URL you submitted by copying the URL from the
 assignment page, then cloning a copy of your repository using that
 URL.  This should be done by a _different_ group member from the one
 who submitted the URL.
 


### How to find your Bitbucket and Trello URLs

#### To find your Trello URL:

#. Visit your board and select "... Show Menu" from the upper right.

    ![Show Menu](trello-menu.png){width=70%}\


#. Select "... More" from the options.

    ![Show Menu](trello-menu-more.png){width=70%}\


#. Copy the url from the "Link to this board" dialog.

    ![Show Menu](trello-menu-url.png){width=70%}\


#### To find your Bitbucket URL:

#. Visit your Bitbucket.org page.

#. Select your repository.

    ![Select Repository](bitbucket-home.png){width=70%}\


#. Select "Clone" in the upper right corner.

    ![Bitbucket Repository](bitbucket-repo.png){width=70%}\


#. Copy the URL _only_ (**not the "git clone" part!**).

    ![Bitbucket Clone](bitbucket-clone.png){width=70%}\
